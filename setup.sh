#!/bin/bash

sudo apt-get install curl wget python-software-properties git build-essential binutils-doc nmap elinks zip ghostscript python-pip mailutils dialog markdown

#
# Add Ansible
#
sudo apt-add-repository -y ppa:ansible/ansible
sudo apt-get update
sudo apt-get install -y ansible

sudo apt-get install php5 php5-cli mysql-server-5.6
